package com.k15t.examples;

import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;

import java.util.Map;
import java.util.function.Supplier;


public class ScrollExportersV4ContentPropertiesPlaceholder {

    private ContentPropertyManager contentPropertyManager;
    private PageManager pageManager;


    public ScrollExportersV4ContentPropertiesPlaceholder(ContentPropertyManager contentPropertyManager, PageManager pageManager) {
        this.contentPropertyManager = contentPropertyManager;
        this.pageManager = pageManager;
    }


    public String render(Map<String, Supplier<String>> context) {
        String pageId = context.get("export.contentId").get();
        String propertyKey = context.get("parameter.propertyKey").get();

        Page page = pageManager.getPage(Long.parseLong(pageId));
        return contentPropertyManager.getTextProperty(page, propertyKey);
    }


    public String getPlaceholderSpec() {
        return "{"
                + "    \"version\": \"1\","
                + "    \"metadata\": {"
                + "        \"title\": \"Content Property Placeholder\","
                + "        \"description\": \"Outputs the value of a content property\","
                + "        \"category\": \"general\""
                + "    },"
                + "    \"parameters\": ["
                + "        {"
                + "            \"key\":\"propertyKey\","
                + "            \"title\": \"Property Key\","
                + "            \"description\": \"The key as used when setting the content property\","
                + "            \"type\": \"string\""
                + "        }"
                + "    ]"
                + "}";
    }

}