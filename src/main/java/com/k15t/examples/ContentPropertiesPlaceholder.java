package com.k15t.examples;


import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;


public class ContentPropertiesPlaceholder {

    private ContentPropertyManager contentPropertyManager;
    private PageManager pageManager;


    public ContentPropertiesPlaceholder(ContentPropertyManager contentPropertyManager, PageManager pageManager) {
        this.contentPropertyManager = contentPropertyManager;
        this.pageManager = pageManager;
    }


    public String get(String pageId, String propertyKey) {
        Page page = pageManager.getPage(Long.parseLong(pageId));
        return contentPropertyManager.getTextProperty(page, propertyKey);
    }
}
